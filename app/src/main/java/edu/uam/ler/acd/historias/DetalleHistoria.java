package edu.uam.ler.acd.historias;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class DetalleHistoria extends AppCompatActivity {

    ProgressDialog progessDialog;
    JSONObject jsonObject;

    TextView nombreTV;
    TextView historiaTV;

    ImageView imageViewdetail;
    String nombre;
    String histora;

    CognitoCachingCredentialsProvider credentialsProvider;
    TransferUtility transferUtility;

    File imageFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        int quien = intent.getIntExtra(TodasLasHistorias.MESSAGE, 0);
        setContentView(R.layout.activity_detalle_historia);

        nombreTV = (TextView)findViewById(R.id.textoDetalleAutor);
        historiaTV = (TextView)findViewById(R.id.textoDetalleHistoria);

        Log.v("UAM", "llegando con " + Integer.toString(quien));

        String url = "http://sample-env-2.zcwip3isma.us-east-1.elasticbeanstalk.com/historia/show/" +
                (quien + 1) + ".json";

        new JsonTask().execute(url);

        // Initialize the Amazon Cognito credentials provider
        credentialsProvider = new CognitoCachingCredentialsProvider(
                getApplicationContext(),
                "us-east-1:1ea0416d-7512-4e83-832a-538a288e0638", // Identity Pool ID
                Regions.US_EAST_1 // Region
        );

        AmazonS3 s3 = new AmazonS3Client(credentialsProvider);
        // s3.setRegion(Region.getRegion(Regions.US_EAST_1));
        transferUtility = new TransferUtility(s3, this);

        imageViewdetail = (ImageView) findViewById(R.id.imageViewdetail);
    }

    public void changeImage(){
        Bitmap bitmap = null;
        Log.v("UAM", "WILL DECODE " + imageFile.getAbsolutePath() + imageFile.exists() + imageFile.length());
        bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
        imageViewdetail.setImageBitmap(bitmap);
    }

    public void obtieneImagen(String filename){

        Log.v("UAM", "descargando " + filename);

        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        imageFile = null;
        try {
            imageFile = File.createTempFile("tmpimg",".jpg", storageDir);
        } catch (IOException e) {
            e.printStackTrace();
        }

        TransferObserver observer = transferUtility.download("historias", filename + ".jpg", imageFile);

        observer.setTransferListener(new TransferListener(){

            @Override
            public void onStateChanged(int id, TransferState state) {
                // do something
                Log.v("UAM", state.toString());

                if(state.equals(TransferState.IN_PROGRESS)){
                    progessDialog = new ProgressDialog(DetalleHistoria.this);
                    progessDialog.setMessage("descargando foto");
                    progessDialog.setCancelable(false);
                    progessDialog.show();
                }

                if(state.equals(TransferState.COMPLETED)){
                    if (progessDialog.isShowing()){
                        progessDialog.dismiss();
                    }
                    changeImage();
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                int percentage = (int) (bytesCurrent/bytesTotal * 100);
                //Display percentage transfered to user
                Log.v("UAM", Integer.toString(percentage));
            }

            @Override
            public void onError(int id, Exception ex) {
                Log.v("UAM", "error de descarga");
                if (progessDialog.isShowing()){
                    progessDialog.dismiss();
                }
            }

        });

    }

    public void  actualizaDatos(){
        try{
            nombre = jsonObject.get("autor").toString();
            histora = jsonObject.get("texto").toString();
            nombreTV.setText(nombre);
            historiaTV.setText(histora);
            obtieneImagen(jsonObject.get("fotoid").toString());
        }
        catch (JSONException e){
        }
    }

    private class JsonTask extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();

            progessDialog = new ProgressDialog(DetalleHistoria.this);
            progessDialog.setMessage("Espera un segundo...");
            progessDialog.setCancelable(false);
            progessDialog.show();
        }

        protected String doInBackground(String... params) {


            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();


                InputStream stream = connection.getInputStream();

                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line = "";

                while ((line = reader.readLine()) != null) {
                    buffer.append(line+"\n");
                    Log.d("Response: ", "> " + line);
                }
                //all response
                return buffer.toString();


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (progessDialog.isShowing()){
                progessDialog.dismiss();
            }
            Log.v("UAM", result);
            try {
                jsonObject = new JSONObject(result);
                actualizaDatos();
            } catch (JSONException e) {
                Log.e("UAMUAM", "el archio no esta bien");
                e.printStackTrace();
            }
        }
    }
}
