package edu.uam.ler.acd.historias;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class TodasLasHistorias extends AppCompatActivity {

    ListView listView;
    ProgressDialog progessDialog;
    ArrayList<String> historias;
    JSONArray datosjson = null;
    ArrayAdapter<String> adapter;

    public final static String MESSAGE = "edu.uam.ler.acd.historias.TodasLasHistorias";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todas_las_historias);

        listView = (ListView) findViewById(R.id.listdehistorias);

        final String url = "http://sample-env-2.zcwip3isma.us-east-1.elasticbeanstalk.com/historia.json?max=1000";

        new JsonTask().execute(url);

        historias = new ArrayList<String>();

        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, historias);

        // Assign adapter to ListView
        listView.setAdapter(adapter);

        // ListView Item Click Listener
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                int itemPosition = position;
                cambiaADetalle(itemPosition);
            }

        });
    }

    private void cambiaADetalle(int quien){
        Intent intent = new Intent(this, DetalleHistoria.class);
        intent.putExtra(MESSAGE, quien);
        startActivity(intent);
    }

    public void  actualizaDatos(){

        try{
           historias.clear();
            for(int i = 0; i < datosjson.length(); i++) {
             JSONObject jo = datosjson.getJSONObject(i);
                if(jo.get("texto").toString().length() < 10)
                    historias.add(jo.get("texto").toString());
                else historias.add(jo.get("texto").toString().substring(0,10) + "...");
            }
            adapter.notifyDataSetChanged();
            //listView.setSelection(adapter.getCount() - 1);
        }
        catch (JSONException e){
        }
    }

    private class JsonTask extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();

            progessDialog = new ProgressDialog(TodasLasHistorias.this);
            progessDialog.setMessage("Espera un segundo...");
            progessDialog.setCancelable(false);
            progessDialog.show();
        }

        protected String doInBackground(String... params) {

            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();


                InputStream stream = connection.getInputStream();

                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line = "";

                while ((line = reader.readLine()) != null) {
                    buffer.append(line+"\n");
                    Log.d("Response: ", "> " + line);
                }
                //all response
                return buffer.toString();


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (progessDialog.isShowing()){
                progessDialog.dismiss();
            }
            Log.v("UAM", result);
            try {
                datosjson = new JSONArray(result);
                Log.e("UAMUAM" , "cant " + datosjson.length());
                actualizaDatos();
            } catch (JSONException e) {
                Log.e("UAMUAM", "el archio no esta bien");
                e.printStackTrace();
            }
        }
    }
}
