package edu.uam.ler.acd.historias;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Button butnuevaHistoria = (Button)findViewById(R.id.buttonNueva);
        butnuevaHistoria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               cambiaAnuevahistoria();
            }
        });

        Button butverHistorias = (Button)findViewById(R.id.buttonHistorias);
        butverHistorias.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cambiaAverHistorias();
            }
        });

        Button butverMapa = (Button)findViewById(R.id.buttonMapa);
        butverMapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cambiaAverMapa();
            }
        });
    }

    private void cambiaAverMapa() {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
    }

    private void cambiaAnuevahistoria(){
        Intent intent = new Intent(this, NuevaHistoria.class);
        startActivity(intent);
    }

    private void cambiaAverHistorias(){
        Intent intent = new Intent(this, TodasLasHistorias.class);
        startActivity(intent);
    }
}

