package edu.uam.ler.acd.historias;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.cognito.CognitoSyncManager;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectListing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class NuevaHistoria extends AppCompatActivity {

    LocationManager locationManager;
    LocationListener locationListener;

    double latitud;
    double longitud;

    EditText textoName;
    EditText textoHistoria;
    ImageView imageView;

    ProgressDialog progessDialog;

  //  String mCurrentPhotoPath;

    static final int REQUEST_TAKE_PHOTO = 1;

    Uri photoURI;
    File photoFile;

    CognitoCachingCredentialsProvider credentialsProvider;
    TransferUtility transferUtility;

    String imageFileName = "none";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nueva_historia);

        Button buttonManda = (Button) findViewById(R.id.buttonMandar);
        buttonManda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                subes3();
            }
        });

        textoName = (EditText) findViewById(R.id.entradaNombre);
        textoHistoria = (EditText) findViewById(R.id.entradaHistoria);

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                Log.v("UAMUAM", "estamos en " + location.getLatitude() + " " + location.getLongitude());
                latitud = location.getLatitude();
                longitud = location.getLongitude();
            }
            public void onStatusChanged(String provider, int status, Bundle extras) {}
            public void onProviderEnabled(String provider) {}
            public void onProviderDisabled(String provider) {}
        };

        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);

        Button buttonFoto = (Button) findViewById(R.id.buttonFoto);
        buttonFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tomarFoto();
            }
        });

        imageView = (ImageView) findViewById(R.id.imageView);

        // Initialize the Amazon Cognito credentials provider
        credentialsProvider = new CognitoCachingCredentialsProvider(
                getApplicationContext(),
                "us-east-1:1ea0416d-7512-4e83-832a-538a288e0638", // Identity Pool ID
                Regions.US_EAST_1 // Region
        );

        AmazonS3 s3 = new AmazonS3Client(credentialsProvider);
       // s3.setRegion(Region.getRegion(Regions.US_EAST_1));
        transferUtility = new TransferUtility(s3, this);
    }

    private void subes3(){

        TransferObserver observer = transferUtility.upload(
                "historias",        // The bucket to upload to
                imageFileName + ".jpg",       // The key for the uploaded object
                photoFile  // The file where the data to upload exists
        );


        observer.setTransferListener(new TransferListener(){

            @Override
            public void onStateChanged(int id, TransferState state) {
                // do something
                Log.v("UAM", state.toString());

                if(state.equals(TransferState.IN_PROGRESS)){
                    progessDialog = new ProgressDialog(NuevaHistoria.this);
                    progessDialog.setMessage("Subiendo foto");
                    progessDialog.setCancelable(false);
                    progessDialog.show();
                }

                if(state.equals(TransferState.COMPLETED)){
                    if (progessDialog.isShowing()){
                        progessDialog.dismiss();
                    }
                    mandaDatos();
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                int percentage = (int) (bytesCurrent/bytesTotal * 100);
                //Display percentage transfered to user
                Log.v("UAM", Integer.toString(percentage));
            }

            @Override
            public void onError(int id, Exception ex) {
                if (progessDialog.isShowing()){
                    progessDialog.dismiss();
                }
                Log.v("UAM", "error de subida");
            }

        });

    }

    private void tomarFoto() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(this, "edu.uam.ler.acd.historias.fileprovider", photoFile);
                Log.v("UAM", photoURI.toString());
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        imageFileName = "JPEG_" + UUID.randomUUID().toString();
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        //mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void mandaDatos() {
        String nombre = textoName.getText().toString().trim();
        String historia = textoHistoria.getText().toString().trim();

        if (nombre.isEmpty() || historia.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Favor de rellenar todos los campos", Toast.LENGTH_LONG).show();
        } else {
            try {
                String nombreCod = URLEncoder.encode(nombre, "utf-8");
                String historiaCod = URLEncoder.encode(historia, "utf-8");
                String fotoidCod = URLEncoder.encode(imageFileName, "utf-8");

                Log.v("UAM", "mandnado con " + (float)latitud + " y " + (float)longitud);
                String url = "http://sample-env-2.zcwip3isma.us-east-1.elasticbeanstalk.com/historia/nuevahistoria?" +
                        "autor=" + nombreCod + "&texto=" + historiaCod +
                        "&lat=" + (float)latitud + "&lng=" + (float)longitud + "&fotoid=" + fotoidCod;
                Log.v("UAM", "foto id sera " + fotoidCod);
                new SubirTask().execute(url);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(locationListener);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), photoURI);
                imageView.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private class SubirTask extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();

            progessDialog = new ProgressDialog(NuevaHistoria.this);
            progessDialog.setMessage("Por favor espera");
            progessDialog.setCancelable(false);
            progessDialog.show();
        }

        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
             //   connection.setRequestMethod("GET");
                connection.connect();


                InputStream stream = connection.getInputStream();

                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line = "";

                while ((line = reader.readLine()) != null) {
                    buffer.append(line+"\n");
                }

                Log.v("UAM", "todo fue leido OKOK" + buffer.toString() + "END") ;
                return buffer.toString();

            } catch (MalformedURLException e) {
                Log.e("UAM", "error de url");
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("UAM", "error de io");
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    Log.e("UAM", "error de io2");
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (progessDialog.isShowing()){
                progessDialog.dismiss();
            }

            if(result.trim().equals("OK")) {
                Toast.makeText(getApplicationContext(), "Historia subida existosamente", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Error de conexion", Toast.LENGTH_LONG).show();
            }
        }
    }
}
